# GRAPHQL

### ======================================================================================

## Reference Documentation

#### ===================================================================================================== 

### Get All Actor and Film of Actor
```
    query(){
       getAllActors{
         actorId
         firstName
         lastName
         address
         film{
           filmId
           name
           lunchYear
         }
       }
    }
   ```
#### ===================================================================================================== 

### Get Actor By Id
```
    query($id:Int){
        getActorById(id:$id){
            actorId
            FIRST_NAME:firstName 
            lastName
            flimId
        }
    }
```
with different Name
```
    query($id:Int){
        getActorById(id:$id){
            actorId
            FIRST_NAME:firstName 
            lastName
            flimId
        }
    }
```
### Query Variables
```
{
  "id": 4
}
```
#### ===================================================================================================== 

### Mutation by id and address
```
    mutation{
        updateActorAddress(id:4,address:"New Address"){
            actorId
            firstName
            lastName
            address
        }
    }

```
#### =====================================================================================================  

### Mutation by object passing

```
    mutation($input:AddressDto){
        updateActorAddressByObject(addressDto:$input){
            actorId
            address
            firstName
            lastName
        }
    }
```
Query Variables
```
    {
      "input": {
        "actorId": 4,
        "address": "New Address Nepal"
      }
    }
```
#### ===================================================================================================== 

### Optional fetch
```
    query($withFilm:Boolean!){
        getAllActors{
            actorId
            firstName
            lastName
            address
            film @include(if:$withFilm){
                filmId
                name
                lunchYear
            }
        }
    }
```
Query Variables
```
    {
      "withFilm": true
    }
```
#### ===================================================================================================== 

### Fragment Example
```
fragment actorDetails on Actor{
    actorId
    firstName
    lastName
}

query($withFilm:Boolean!){
    getAllActors{
        ...actorDetails
        address
        film @include(if:$withFilm){
            filmId
            name
            lunchYear
        }
    }
}
```
Query Variables
```
    {
      "withFilm": true
    }
```
