package com.example.graphqldemo1.model;

import graphql.schema.GraphQLInputType;
import lombok.Data;

@Data
public class AddressDto implements GraphQLInputType {


    @Override
    public String getName() {
        return "addressUpdate";
    }
    private Integer actorId;
    private String address;

}
