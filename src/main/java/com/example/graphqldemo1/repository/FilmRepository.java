package com.example.graphqldemo1.repository;

import com.example.graphqldemo1.model.Film;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FilmRepository extends JpaRepository<Film,Integer> {
}
