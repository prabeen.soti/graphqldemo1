package com.example.graphqldemo1.repository;

import com.example.graphqldemo1.model.Actor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActorRepository extends JpaRepository<Actor,Integer> {
}
