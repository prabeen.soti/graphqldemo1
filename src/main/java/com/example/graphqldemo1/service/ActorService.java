package com.example.graphqldemo1.service;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.coxautodev.graphql.tools.GraphQLSubscriptionResolver;
import com.example.graphqldemo1.model.Actor;
import com.example.graphqldemo1.model.AddressDto;
import com.example.graphqldemo1.repository.ActorRepository;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;

import javax.transaction.Transactional;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ActorService implements GraphQLQueryResolver, GraphQLMutationResolver, GraphQLSubscriptionResolver {

    @Autowired
    private ActorRepository actorRepository;

    private ConcurrentHashMap<Integer, FluxSink<Actor>> subscribers = new ConcurrentHashMap<>();

    public List<Actor> getAllActors(){
        return actorRepository.findAll();
    }

    public Actor getActorById(Integer id){
        return actorRepository.findById(id).get();
    }

    @Transactional
    public Actor updateActorAddress(Integer id,String address){
        Actor actor = actorRepository.findById(id).get();
        actor.setAddress(address);
        actorRepository.save(actor);
        if(subscribers.get(id)!=null){
            subscribers.get(id).next(actor);
        }
        return actor;
    }

    @Transactional
    public Actor updateActorAddressByObject(AddressDto address){
        Actor actor = actorRepository.findById(address.getActorId()).get();
        actor.setAddress(address.getAddress());
        actorRepository.save(actor);
        if(subscribers.get(address.getActorId())!=null){
            subscribers.get(address.getActorId()).next(actor);
        }
        return actor;
    }

    public Publisher<Actor> onActorUpdate(Integer actorId){
        return Flux.create(subscriber->subscribers.put(actorId,subscriber.onDispose(()->subscribers.remove(actorId,subscriber))),FluxSink.OverflowStrategy.LATEST);
    }
}
