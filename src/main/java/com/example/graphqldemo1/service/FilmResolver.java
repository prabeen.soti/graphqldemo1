package com.example.graphqldemo1.service;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.example.graphqldemo1.model.Actor;
import com.example.graphqldemo1.model.Film;
import com.example.graphqldemo1.repository.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class FilmResolver implements GraphQLResolver<Actor> {

    @Autowired
    private FilmRepository filmRepository;

    @Transactional
    public Film getFilm(Actor actor){
        return filmRepository.findById(actor.getFlimId()).get();
    }

}
