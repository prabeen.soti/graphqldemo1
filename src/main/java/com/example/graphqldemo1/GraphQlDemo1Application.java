package com.example.graphqldemo1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GraphQlDemo1Application {

    public static void main(String[] args) {
        SpringApplication.run(GraphQlDemo1Application.class, args);
    }

}
